# Astonish Postmaster #

Read emails from a [Redis](http://redis.io) queue and send them via [MailGun](http://www.mailgun.net). This app stands on
its own, but we use it for our Drupal sites with the [astonish_mail module](http://drupal.org/project/astonish_mail).

## Dependencies ##
* Redis
* MongoDB (used for logging, but can be replaced)
* A Mailgun account

## Installation ##
1. Install and configure dependencies

    In MongoDB create a database "mailqueue" with a capped collection called "log." [helpful blogpost](http://blog.mongodb.org/post/116405435/capped-collections)

        $> use mailqueue
        $> db.createCollection("log", {capped: true, size: 10000})

2. Create a config.json file in this project's folder
3. Provide Redis and Mailgun credentials in the config.json file.

        {
          "redis": {
            "port": 6379,
            "host": "localhost",
            "key": ""
          },
          "smtp": {
            "host": "smtp.mailgun.org",
            "port": 587,
            "auth": {
              "user": "",
              "pass": ""
            }
          }
        }
4. Run: **npm install** in this project's folder

## Usage ##
Run: **node bin/server**

This will start a process where every second an email is popped off the Redis
queue and sent through Mailgun.

I recommend starting this script in the background and keeping it running
continually. One tool that can be used for this is [Forever](https://blog.nodejitsu.com/keep-a-nodejs-server-up-with-forever).
