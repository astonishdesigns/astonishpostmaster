module.exports = MailProcessor;

function MailProcessor (options) {
  var options = options || {};

  var redisClient = options.redisClient;
  var transport = options.transport;
  var logger = options.logger;

  /*
   * Process Mail Queue
   *
   * Publically accessible method that gets the length of the redis mail queue
   * and then passes the result to the private callback: popMailFromQueue().
   */
  this.processMailQueue = function() {
    redisClient.LLEN('mail', popMailFromQueue);
  }

  /****************************************************************************
   * PRIVATE METHODS
   ****************************************************************************/

  /*
   * Pop Mail From Queue
   *
   * If there is no error and there are messages in the queue (lenth is > 0),
   * then pop the message off the mail queue and pass the result to the private
   * callback: sendMail().
   */
  function popMailFromQueue(err, queueLength) {
    if (!err) {
      if (queueLength > 0 && queueLength !== false) {
        redisClient.LPOP('mail', sendMail);
      }
      else {
        //logger.log('info', 'Mail queue is empty');
      }
    }
  }

  /*
   * Send Mail
   *
   * Given a mail object, JSON parse the mail and send it through the transport.
   */
  function sendMail(err, mail) {
    var data = JSON.parse(mail);

    logger.log('info', 'Popped mail', {
      from: data.from,
      to: data.to,
      subject: data.subject
    });

    if (mail !== null) {
      transport.sendMail(data, logResponse);
    }
  }

  function logResponse(err, res) {
    if (!err) {
          logger.log('info', res.message);
        }
        else {
          logger.log('error', err);
        }
  }
}


