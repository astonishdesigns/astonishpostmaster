Pops emails off a Redis queue and sends them through mailgun.

* Dependencies *
1. Redis
2. Email transport (nodemailer)
3. Winston (or some other logger)

* Usage *
processor = require('mailProcessor');

options = {
  redisClient: <object>,
  transport: <object>,
  logger: <object>,
}

processor = new Processor(options);

processor.processMailQueue();
